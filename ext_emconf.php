<?php

/*
 * Extension Manager configuration file for ext "sr_feuser_register".
 */

$EM_CONF[$_EXTKEY] = [
	'title' => 'Front End User Registration',
	'description' => 'A self-registration variant of Kasper Skårhøj\'s Front End User Admin extension for TYPO3 CMS.',
	'category' => 'plugin',
	'state' => 'stable',
	'clearCacheOnLoad' => 1,
	'author' => 'Stanislas Rolland',
	'author_email' => 'typo3AAAA@sjbr.ca',
	'author_company' => 'SJBR',
	'version' => '13.4.0',
	'constraints' => [
		'depends' => [
			'typo3' => '13.4.0-13.4.99',
			'felogin' => '13.4.0-13.4.99',
			'static_info_tables' => '13.4.0-13.4.99'
		],
		'conflicts' => [
		],
		'suggests' => [
			'sr_freecap' => '13.4.2-13.4.99'
		]
    ],
    'autoload' => [
        'psr-4' => [
        	'SJBR\\SrFeuserRegister\\' => 'Classes'
        ]
    ]
];