<?php
defined('TYPO3') or die();

call_user_func(
    function($extKey)
    {
		/**
		 * Enabling localization of frontend groups
		 */
		$GLOBALS['TCA']['fe_groups']['ctrl']['languageField'] = 'sys_language_uid';
		$GLOBALS['TCA']['fe_groups']['ctrl']['translationSource'] = 'l10n_source';
		$GLOBALS['TCA']['fe_groups']['ctrl']['transOrigPointerField'] = 'l10n_parent';
		$GLOBALS['TCA']['fe_groups']['ctrl']['transOrigDiffSourceField'] = 'l10n_diffsource';
		$GLOBALS['TCA']['fe_groups']['columns']['subgroup']['l10n_mode'] = 'exclude';
		$GLOBALS['TCA']['fe_groups']['columns']['subgroup']['config']['foreign_table_where'] = ' AND fe_groups.sys_language_uid IN (-1,0) AND NOT(fe_groups.uid = ###THIS_UID###) AND fe_groups.hidden=0 ORDER BY fe_groups.title';
		
		$addColumnArray = [
			'sys_language_uid' => [
				'exclude' => true,
				'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
				'config' => [
					'type' => 'language'
				]
			],
			'l10n_parent' => [
				'displayCond' => 'FIELD:sys_language_uid:>:0',
				'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'relationship' => 'manyToOne',
					'items' => [
						[
							'label' => '',
							'value' => 0
						]
					],
					'maxitems' => 1,
					'foreign_table' => 'fe_groups',
					'foreign_table_where' => 'AND {#fe_groups}.{#pid}=###CURRENT_PID### AND {#fe_groups}.{#sys_language_uid} IN (-1,0)',
					'default' => 0
				]
			],
			'l10n_source' => [
				'config' => [
					'type' => 'passthrough',
					'default' => ''
				]
			],
			'l10n_diffsource' => [
				'config' => [
					'type' => 'passthrough',
					'default' => ''
				]
			]
		];
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', $addColumnArray);
	},
	'sr_feuser_register'
);