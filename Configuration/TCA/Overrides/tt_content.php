<?php
defined('TYPO3') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(
    function($extKey)
    {
		$pluginSignature = 'sr_feuser_register_pi1';
		ExtensionManagementUtility::addPlugin(
			 [
				 'LLL:EXT:sr_feuser_register/Resources/Private/Language/locallang_db.xlf:tt_content.list_type',
				 $pluginSignature,
				 'ce-sjbr-feuser-register',
				 'forms'
			 ],
			 'CType',
			 $extKey
		 );
		ExtensionManagementUtility::addPiFlexFormValue('*', 'FILE:EXT:sr_feuser_register/Configuration/FlexForms/flexform_ds_pi1.xml', $pluginSignature);
		// Activate the display of the FlexForm field
		// Add the FlexForm to the show item list
		ExtensionManagementUtility::addToAllTCAtypes(
			'tt_content',
			'--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:plugin, pi_flexform,pages',
			$pluginSignature,
			'after:palette:headers'
		);
		
		$GLOBALS['TCA']['tt_content']['columns']['fe_group']['config']['foreign_table_where'] = ' AND fe_groups.sys_language_uid IN (-1,0) ORDER BY fe_groups.title';
	},
	'sr_feuser_register'
);