<?php
use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;
return [
   // Icon identifier
   'ce-sjbr-feuser-register' => [
	   // Icon provider class
	   'provider' => SvgIconProvider::class,
	   // The source SVG for the SvgIconProvider
	   'source' => 'EXT:sr_feuser_register/Resources/Public/Icons/Extension.svg'
   ]
];